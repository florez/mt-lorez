import numpy as np

from createSim import createSim

fck = 50




kRiss_values = 0.1
fsy, fcd, taucd = np.zeros(15), np.zeros(15), np.zeros(15)

fsy[:5] = np.linspace(391, 450, 5, endpoint=False)
fsy[5:10] = np.linspace(450, 550, 5, endpoint=False)
fsy[10:] = np.linspace(550, 650, 5)
fsu = 1.05 * fsy

fcd[:5] = np.linspace(28, 50, 5, endpoint=False)
fcd[5:10] = np.linspace(50, 59, 5, endpoint=False)
fcd[10:] = np.linspace(59, 68, 5)

taucd = .3 * np.sqrt(fcd)
taucd[:5] = np.linspace(.3 *np.sqrt(50)/1.5, .3 * np.sqrt(50), 5, endpoint=False)

esu = 0.045
# esu = np.zeros_like(fsy)
# esu = np.linspace(.045, .085, 5, endpoint=False)
# esu[5:10] = np.linspace(.085, .100, 5, endpoint=False)
# esu[10:] = np.linspace(.1, .115, 5)



for i, _ in enumerate(fcd):
    

    parameters = {
            'kRiss_P':      kRiss_values,
            'fsy_P':        fsy[i],
            'fsu_P':        fsu[i],
            'fcd_P':        fcd[i],
            'fck_P':        fck,
            'tau_cd':       taucd[i],
            'D_max':        32,
            'ecu_P':        -0.002,
            'eu_act_P':     esu,
            'stepsize_P':   0.10,
            'start_eta':    1.8
        }

    createSim(par=parameters)
