import json
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import json

from setuptools import setup

from PyCMM import ANSYSBatchCaller
from Parameterfile import parameters


def createSim(par=parameters):
    
    setup = {
        'ansys_path' : 'C:\\Program Files\\ANSYS Inc\\v221\\ansys\\bin\\winx64\\ANSYS221.exe',
        'lic_str' : 'aa_t_a', # License
        'cpus' : 2
    }

    out_dir = 'sim_inputs'

    # get current sim number --------------------------
    try:
        with open('sim_counter.txt', 'r') as f:
            simnumber = int(f.read())
    except FileNotFoundError: 
        simnumber = 0
    simname = f'sim_{simnumber:03d}'
    simnumber += 1
    with open('sim_counter.txt', 'w') as f:
        f.write(str(simnumber))
    # -------------------------------------------------
    
    Caller = (ANSYSBatchCaller(simname)
        .initialize('./APDLmodelTraglast', input_file='00_Mainfile.inp', out_dir=out_dir)
        .writeParameterFile('12.2_Parameters.inp', par)
        .writeSetup(setup)
    )
    


    


if __name__=='__main__':

    createSim()