import multiprocessing
import subprocess, os, fnmatch

folder = os.path.abspath('sim_inputs')
paralell = 3

def singleCall(name):
    path_to = os.path.join(folder, name)
    call_string = os.path.join(path_to, 'Run.py')
    print(f'running sim {name}')

    try: subprocess.run(f'py {call_string} {path_to}')
    except KeyboardInterrupt: return

    # Delete large files again, comment this if you need it
    for root, subdir, files in os.walk(os.path.join(folder, name)):
        for file in fnmatch.filter(files, 'sim*.r*'):
            os.remove(os.path.join(root, file))
        for file in fnmatch.filter(files, 'sim*.esav*'):
            os.remove(os.path.join(root, file))
        for file in fnmatch.filter(files, 'Solu_.db'):
            os.remove(os.path.join(root, file))

    return


if __name__=='__main__':

    pool = multiprocessing.Pool(processes=paralell)
    tasks = (sim for sim in os.listdir(folder))
    r = pool.map_async(singleCall, tasks)
    r.wait()
    pool.close()

