import enum
import numpy as np

from createSim import createSim

from uqpylab import sessions

fck = 50

mySession = sessions.cloud()
uq = mySession.cli
uq.rng(100)

# Mean values
fccm = 59
fctm = 0.3 * 50**(2/3)
fsym = 550
fsum = 580

# Marginal distributions X_i
InpOptions = {
    'Marginals': [
        {
            'Name': 'fcc',
            'Type': 'Lognormal',
            'Parameters': [np.log(fccm), 0.06]
        },
        {
            'Name': 'ecu',
            'Type': 'Lognormal',
            'Parameters': [np.log(0.002), 0.15]
        },
        {
            'Name': 'kRiss',
            'Type': 'Uniform',
            'Parameters': [0.1, 1]
        },
        # {
        #     'Name': 'fct',
        #     'Type': 'Lognormal',
        #     'Parameters': [np.log(fctm), 0.30]
        # },
        {
            'Name': 'fsy',
            'Type': 'Lognormal',
            'Parameters': [np.log(fsym), 0.033]
        },
        {
            'Name': 'esu',
            'Type': 'Lognormal',
            'Parameters': [np.log(0.10), 0.05]
        }
    ]
}

myInput = uq.createInput(InpOptions)

X = uq.getSample(N=60, Method='LHS')

# ------------------------------------------------------------


kRiss_values = X[:, 2]
fsy = X[:, 3]
fsu = 1.05*fsy
fcd = X[:, 0]
taucd = .3*np.sqrt(fcd)
esu = X[:, 4]
ecu = X[:, 1]*-1



for i in range(X.shape[0]):


    parameters = {
            'kRiss_P':      kRiss_values[i],
            'fsy_P':        fsy[i],
            'fsu_P':        fsu[i],
            'fcd_P':        fcd[i],
            'fck_P':        fck,
            'tau_cd':       taucd[i],
            'D_max':        32,
            'ecu_P':        ecu[i],
            'eu_act_P':     esu[i],
            'stepsize_P':   0.10,
            'start_eta':    2.0
        }

    createSim(par=parameters)