import numpy as np

from createSim import createSim

fck = 50

kRiss = 0.5

fsy = np.arange(350, 675, 25)
fsu = 1.05 * fsy

fcd = 59 # np.linspace(28, 68, 6)
taucd = .3*np.sqrt(fcd) # np.linspace(1.41, 2.5, 6)

esu = 0.100

for i, _ in enumerate(fsy):

    parameters = {
            'kRiss_P':      kRiss,
            'fsy_P':        fsy[i],
            'fsu_P':        fsu[i],
            'fcd_P':        fcd,
            'fck_P':        fck,
            'tau_cd':       taucd,
            'D_max':        32,
            'ecu_P':        -0.002,
            'eu_act_P':     esu,
            'stepsize_P':   0.10,
            'start_eta':    2.8
        }

    createSim(par=parameters)