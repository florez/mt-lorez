import os, shutil, subprocess, json
from pathlib import Path
import importlib.resources 

from . import templates


class ANSYSBatchCaller(object):
    """
    Basic object to run ANSYS files in batch mode.
    """

    def __init__(self, name) -> None:
        self.name = name

    def initialize(self, dir_model, input_file, out_dir) -> None:
        """
        Initialize the caller.

        args:   - dir_model: str = path to model files
                - input_file: str = main APDL file
        """
        Path(out_dir).mkdir(parents=True, exist_ok=True)
        self.work_dir = os.path.join(out_dir, f'{self.name}')
        self.createWorkDirectory(self.work_dir, dir_model)
        self.input_file = os.path.join(self.work_dir, input_file)
        self.output_file = os.path.join(self.work_dir, f'{self.name}_output.out')

        self.loadSetup() # loads setup file, else default values

        return self


    def setAnsysPath(self, path) -> None:
        """
        Set path to ANSYS executable.
        """
        self._ansys_path = path

        return self


    def createWorkDirectory(self, work_dir, dir_model):
        """
        Create directory for calculation.
        """
        # Create work directory
        if os.path.exists(work_dir): 
            ans = input('This directory does already exist, do you want to overwrite it? (y/n)')
            if ans in ('yes', 'y'):
                shutil.rmtree(work_dir)
            else:
                raise RuntimeError('Abort. Directory exists!')
        os.mkdir(work_dir)

        # Copy inp files
        for file in os.listdir(dir_model):
            shutil.copy(os.path.join(dir_model, file), os.path.join(work_dir, file))

        return self

        
    def launchAnsysBatch(self):
        """
        Assemble command and execute.
        """
        # Ansys batch mode command
        launch_string = f""""{self._ansys_path}" -b -p {self._lic_str} -np {self._cpus} -dir "{self.work_dir}" -j {self.name} -s read -l en-us -i "{self.input_file}" -o "{self.output_file}" -m 2000 -db 1000 """

        # Execute command
        subprocess.run(launch_string)

        return self


    def writeParameterFile(self, filename, parameters: dict):
        """
        Take the input vector and store it to a txt file for APDL to read.
        """
        parameters_list = [[key, parameters[key]] for key in parameters.keys()]
        names = [i[0] for i in parameters_list]
        values = [i[1] for i in parameters_list]
        
        filename = os.path.join(self.work_dir, filename)
        
        header = importlib.resources.read_text(templates, 'header_parameterfile.txt')
        with open(filename, 'a') as f:
            f.write(header)
            f.write(f'\n! Sim-name: {self.name}\n')
            for name, value in zip(names, values):
                f.write(f'{name} = {value:.4f}\n')

        return self

    def writeSetup(self, setup):
        with open(os.path.join(self.work_dir, 'setup.inp'), 'w') as f:
            f.write(json.dumps(setup))

        return self

    def loadSetup(self):
        try:
            with open(os.path.join(self.work_dir, 'setup.inp'), 'r') as f:
                setup = json.loads(f.read())
        except FileNotFoundError:
            setup = {}

        self._ansys_path = setup.get('ansys_path','C:\\Program Files\\ANSYS Inc\\v221\\ansys\\bin\\winx64\\ANSYS221.exe')
        self._lic_str = setup.get('lic_str', 'aa_t_a') # License
        self._cpus = setup.get('cpus', 1)

        return self





class ANSYSBatchSimple(ANSYSBatchCaller):
    def __init__(self, name, work_dir) -> None:
        super().__init__(name)
        self.work_dir = work_dir
        self.input_file = os.path.join(self.work_dir, '00_Mainfile.inp')
        self.output_file = os.path.join(self.work_dir, f'{self.name}_output.out')

        print('name='+self.name)

        self.loadSetup()
