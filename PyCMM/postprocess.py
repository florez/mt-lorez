"""
Author: florez@ethz.ch
Date: 13.03.22

Based on Matlab versions. Plotting routines for CMM-Usermat. 
"""

import numpy as np
import os
import pandas as pd
from scipy.interpolate import griddata
from scipy.optimize import root


class ResultReader(object):
    """
    Read ANSYS results.
    """

    _delimiter = 'Blank'
    _possible_plate_names = ['D0Platte', 'D1Platte', 'DTPlatte']
    _parameter_file = '12.2_Parameters.inp'
    # _parameter_file = '12.2_Resistance_Parameter.inp'
    _figurepath = 'figures'
    

    def __init__(self, directory) -> None:
        self.directory = directory
        self.plate_name = self.getValidPlateName()
        self.readParameterFile()
        self.name = os.path.basename(directory)


    def getValidPlateName(self) -> str:
        """
        Depending on k_riss, the files are named differently.
        """
        for name in self._possible_plate_names:
            if os.path.isfile(os.path.join(self.directory, f'{name}_mvn.txt')):
                return name


    def readParameterFile(self) -> None:
        parameters = {}
        with open(os.path.join(self.directory, self._parameter_file)) as file:
            for line in file:
                if '=' in line:
                    key, val = line.strip().split('=')
                    key = key.strip()
                    parameters[key] = float(val)

        self.parameters = parameters

    
    def readAll(self) -> None:
        self.readConcrete()
        self.readMVN()
        self.readShear()
        self.readSteel()
        self.shearCheck(out=False)
        self.getFailure()
        
        return self


    def readMVN(self) -> dict:

        file = os.path.join(self.directory, f'{self.plate_name}_mvn.txt')

        arr = np.loadtxt(file)
        
        data = {} # entries: (name, idx, scaling factor)
        data_names = [('mx', 1, 1e-3), ('my', 2, 1e-3), ('mxy', 3, 1e-3), ('vx', 4, 1), ('vy', 5, 1), ('nx', 7, 1), ('ny', 8, 1), ('nxy', 9, 1)]
        for (key, idx, scaling) in data_names:
            data[key] = arr[:, idx].flatten() * scaling

        data['coords'] = arr[:, 10:]
        data['v0'] = np.sqrt(data['vx']**2 + data['vy']**2)
        data['theta_0'] = np.arctan(data['vy']/data['vx'])
        self.mvn = data

        return data

    def readShear(self) -> dict:

        fileVRD = os.path.join(self.directory, f'{self.plate_name}_vrd.txt')
        fileVNW = os.path.join(self.directory, f'{self.plate_name}_vNW.txt')
        arr2 = np.loadtxt(fileVNW)

        data = {}
        data['v_rd'] = arr2[:, 1]
        data['v_0d'] = arr2[:, 2]
        data['v_xd'] = arr2[:, 3]
        data['v_yd'] = arr2[:, 4]
        data['d_v'] = arr2[:, 5]
        data['eps_06d'] = arr2[:, 7]
        data['x_v'] = arr2[:, 10]
        data['y_v'] = arr2[:, 11]
        data['v_rd'][data['v_rd']==-1] = 1e9
        data['theta_0'] = np.arctan(data['v_yd']/data['v_xd'])
        data['v_aus'] = data['v_0d']/data['v_rd']

        self.shear = data
        
        return data

    def getEps06(self, index, dv=340):
        """
        Reverse calculate eps_06d from Vrd, 
        this is more accurate then file due to rounding error.
        """
        def func(eps_v06d, vrd_actual, dv=dv):
            tau_cd = self.parameters['tau_cd']
            kg = 48 / (16 + self.parameters['D_max'])
            kd = 1 / (1 + 2.5*eps_v06d*dv*kg)
            v_rd = kd * tau_cd * dv
            return v_rd - vrd_actual

        eps_v06d_crit = root(func, 1e-2, self.shear['v_rd'][index]).x[0]
        return eps_v06d_crit
        
        
    def readSteel(self) -> dict:
        # 1. bot x, 2. bot y, 3. top y, 4. top x
        coord_layer = {}
        for name in ['bot_x', 'bot_y', 'top_x', 'top_y']:
            file = os.path.join(self.directory, f'{self.plate_name}_coor_layer_{name}.txt')
            coord_layer[name] = np.loadtxt(file)
        
        file = os.path.join(self.directory, f'{self.plate_name}_sig_sr.txt')
        sig_sr = np.loadtxt(file)

        data = {}
        data['bot_x'] = (
            coord_layer['bot_x'],  # Coords
            np.sum(sig_sr[:, 0:2], axis=1), # Stresses x
            np.zeros(sig_sr.shape[0]),  # Stresses y
            np.zeros(sig_sr.shape[0])   # Stresses xy
            )
        data['bot_y'] = (
            coord_layer['bot_y'], 
            np.zeros(sig_sr.shape[0]),
            np.sum(sig_sr[:, 2:4], axis=1),  
            np.zeros(sig_sr.shape[0])
            )
        data['top_x'] = (
            coord_layer['top_x'], 
            np.sum(sig_sr[:, 6:8], axis=1), 
            np.zeros(sig_sr.shape[0]), 
            np.zeros(sig_sr.shape[0])
            )
        data['top_y'] = (
            coord_layer['top_y'], 
            np.zeros(sig_sr.shape[0]), 
            np.sum(sig_sr[:, 4:6], axis=1), 
            np.zeros(sig_sr.shape[0])
            )

        self.steel = data
        return data

    def readConcrete(self) -> dict:

        data = {}
        eps = np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_eps_princ.txt'))
        sig_c = np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_sig_c.txt'))

        data['top'] = {
            'coords': np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_coor_intp_toplayer.txt'))[:, :2],
            'eps': eps[:, 0:3],
            'sigma_c1': sig_c[:, 1], 'sigma_c3': sig_c[:, 2]
        }
        data['bot'] = {
            'coords': np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_coor_intp_botlayer.txt'))[:, :2],
            'eps': eps[:, 12:15],
            'sigma_c1': sig_c[:, 9], 'sigma_c3': sig_c[:, 10]
        }

        self.conc = data
        return data


    def shearCheck(self, out=True):
        """
        Return: list of indices in the shear check area.
        also stores the info about the critical element in self.critElement
        """
        data = self.shear
        _xlim, _ylim = [0, 9700], [0, 5400]
        y_crit = 379 + 200
        bot_line, top_line = _ylim[0] + y_crit, _ylim[1] - y_crit
        xi, yi = data['x_v'], data['y_v']

        mask = np.where((yi<top_line) & (yi>bot_line), True, False)
        masked_vaus = np.ma.array(data['v_aus'], mask=~mask)
        el_max = masked_vaus.argmax()

        eps_v06d_crit = self.getEps06(el_max)

        self.critElement = {
            'v_aus':    data['v_aus'][el_max],
            'theta_0':  data['theta_0'][el_max],
            'v_d':      data['v_0d'][el_max],
            'v_rd':     data['v_rd'][el_max],
            'El_nb':    el_max,
            'location': (xi[el_max], yi[el_max]),
            'eps_v06':  eps_v06d_crit,
            'tau_cd':   self.parameters['tau_cd']
        }
        
        if out: return mask
        else: return None

    def getSigSr(self, method='linear', **kwargs):
        """
        Calculates the steel stress in element quad points (shear check).
        """
        s_x, s_y = self.steel['top_y'][0][:,:2].T
        v_x, v_y = self.shear['x_v'], self.shear['y_v']
        sig_sr = self.steel['top_y'][2]
        self.shear['sig_sr_top'] = griddata((s_x, s_y), sig_sr, (v_x, v_y), method=method, **kwargs)
        return self

    def getSigC3(self, method='linear', **kwargs):
        s_x, s_y = self.steel['top_y'][0][:,:2].T
        v_x, v_y = self.shear['x_v'], self.shear['y_v']
        sig_c3 = self.conc['bot']['sigma_c3']
        sig_c1 = self.conc['bot']['sigma_c1']
        self.shear['sig_c3_bot'] = griddata((s_x, s_y), sig_c3, (v_x, v_y), method=method, **kwargs)
        self.shear['sig_c1_bot'] = griddata((s_x, s_y), sig_c1, (v_x, v_y), method=method, **kwargs)
        return self
    

    def getEpsSr(self):
        fsy, fsu, esu = self.parameters['fsy_P'], self.parameters['fsu_P'], self.parameters['eu_act_P']
        Youngs = 200e3

        s_x, s_y = self.steel['top_y'][0][:,:2].T
        v_x, v_y = self.shear['x_v'], self.shear['y_v']
        sig_sr = self.steel['top_y'][2]

        eps_sr = np.zeros_like(sig_sr)
        eps_sr[sig_sr<=fsy] = sig_sr[sig_sr<=fsy] / Youngs # Elastic
        eps_sr[sig_sr>fsy]  = sig_sr[sig_sr>fsy]  / Youngs + (esu-fsy/Youngs)/(fsu-fsy) * (sig_sr[sig_sr>fsy]-fsy) # Yielding/hardening

        eps_sr_vcoords = griddata((s_x, s_y), eps_sr, (v_x, v_y))

        return eps_sr_vcoords

    @staticmethod
    def getPrincipal(mx: float, my: float, mxy: float) -> np.array:
        """
        Creates 2d tensor and returns eigenvalues and eigenvectors.
        """
        tensor = np.array([
            [mx, mxy/2],
            [mxy/2, my]
        ])
        return np.linalg.eig(tensor)

    def getPrincMembrane(self):
        nx, ny, nxy = [self.mvn[field] for field in ['nx', 'ny', 'nxy']]
        n3 = np.array([self.getPrincipal(nx, ny, nxy)[0].min() for nx, ny, nxy in zip(nx, ny, nxy)])
        return n3

    # -----------------------------------------------------------------------
    # result file
    # -----------------------------------------------------------------------
    def getFailure(self):
        
        file = os.path.join(self.directory, 'failure_infos.txt')
        
        with open(file, 'r') as f:
            lines = [line for line in f]

        def getTime(lines):
            for line in lines:
                if 'TIME STEP AT FAILURE' in line:
                    return float(line.split()[-1])
            return ValueError

        def getFailureMode(lines):
            for line in lines[5:]:
                splitted = line.split() # first word of line
                for i in splitted:
                    try: 
                        value = float(i)
                        if value>0: return splitted[0]
                    except ValueError: None
            return 'No failure detected!' 

        def getElement(lines):
            for line in lines:
                if 'failed element' in line:
                    el = [float(i) for i in line.split() if not i.isalpha()][0]
                    return el
            return '-'

        time = getTime(lines)
        loadfactor_eta = self.parameters['start_eta'] + (time-2)*self.parameters['stepsize_P']

        self.failureInfo = {
            'eta':          loadfactor_eta,
            'failure_mode': getFailureMode(lines),
            'failure_element': getElement(lines)
        }
        
        return


    def openPlot(self, type: str = 'v'):
        """
        Open the pdf with the corresponding plot
        args: type = 'v', 'm', 'c', 's', 'n', 'v_detail'
        """
        path = os.path.join(self.directory, self._figurepath)

        # types
        if type=='v': substring = 'v_check'
        elif type=='m': substring = 'm'
        elif type=='c': substring = 'sig_c3'
        elif type=='s': substring = 'sig_sr'
        elif type=='n': substring = 'n'
        elif type=='v_detail': substring = 'v'
        else: raise ValueError(f'{type} is not a valid input!')

        figure = os.path.join(path, f'{self.name}_{substring}.pdf')
        os.startfile(figure)

        return

    def openAllPlots(self):
        for type in ['v', 'm', 'c', 's', 'n', 'v_detail']:
            self.openPlot(type)
        return
        


class LoadFolder(object):

    def __init__(self) -> None:
        pass

    def load(self, folder, sim_list=None):
        if sim_list is None:
            sim_list = os.listdir(folder)
        else:
            sim_list = [i for i in sim_list]
            if type(sim_list[0])!=str:
                sim_list = self.simList(sim_list)
        self.sims = [os.path.join(folder, i) for i in sim_list]
        
        self.results = [ResultReader(path).readAll() for path in self.sims]

        return self

    def stillRunning(self, folder, sim_list=None):
        if sim_list is None:
            sim_list = os.listdir(folder)
        else:
            sim_list = [i for i in sim_list]
            if type(sim_list[0])!=str:
                sim_list = self.simList(sim_list)
        self.sims = [os.path.join(folder, i) for i in sim_list]

        self.results = []
        for path in self.sims:
            r = ResultReader(path)
            r.readParameterFile()
            try:
                r.getFailure()
            except FileNotFoundError: 
                r.failureInfo = {
                    'eta':          '-',
                    'failure_mode': '-',
                    'failure_element': '-'
                }
            self.results.append(r)

        return self


    @property
    def info(self):

        lst = []
        for i in self.results:
            d = {'name': i.name} | i.failureInfo | i.parameters
            lst.append(d)

        df = pd.DataFrame.from_records(lst)

        return df
    
    @property
    def shear(self):
        lst = []
        for i in self.results:
            d = {'name': i.name} | i.critElement
            lst.append(d)
        df = pd.DataFrame.from_records(lst)

        return df

    @property
    def df(self):
        combined = pd.concat([self.info, self.shear.drop('name', axis=1)], axis=1)
        return combined.loc[:,~combined.columns.duplicated()] 

    def simList(self, lst):
        return [f'sim_{i:03d}' for i in lst]

    @property
    def coords(self):
        return self.results[0].mvn['coords']

    @property
    def coords_steel(self):
        return self.results[0].steel['top_y'][0]