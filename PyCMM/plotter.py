import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.interpolate import griddata
from copy import copy

from PyCMM.postprocess import ResultReader

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------

class MVNPlotter(object):
    """
    M, N, V plots.
    """

    _geo_frame = np.array([[0, 0], [9700, 0], [9700, 5400], [0, 5400], [0, 0]])
    _xlim, _ylim = [0, 9700], [0, 5400]
    _half_line = np.array([[0, 5400/2], [9700, 5400/2]])


    def __init__(self, data: ResultReader, ndivx: int = 25, ndivy: int = 14) -> None:
        self.ndivx, self.ndivy = ndivx, ndivy
        self.data = data
        self.xi, self.yi = data.mvn['coords'][:, :2].T

    def plotFrame(self, ax):
        # Frame
        ax.plot(self._half_line.T[0], self._half_line.T[1], linewidth=1, alpha=1, color='k', linestyle=(0, (7, 5)))
        [ax.hlines(i, *self._xlim, color='k') for i in self._ylim]
        [ax.vlines(i, *self._ylim, color='k') for i in self._xlim]
        ax.set_xlim(self._xlim); ax.set_ylim(self._ylim)

    def createGridData(self, xi: list, yi: list, zi: list, factor=1, interpolation_method: str = 'linear') -> np.array:
        """
        Interpolate the data onto a regular grid in the X, Y plane.
        """
        X, Y = np.meshgrid(
            np.linspace(xi.min(), xi.max(), factor*self.ndivx),
            np.linspace(yi.min(), yi.max(), factor*self.ndivy)
        )
        Z = griddata((xi, yi), zi, (X, Y), method=interpolation_method)

        return X, Y, Z

    @staticmethod
    def _getUnitfromField(field):
        if field[0] in ('m', 'M'):
            return 'MNm/m'
        else:
            return 'MN/m'

    @staticmethod
    def getPrincipal(mx: float, my: float, mxy: float) -> np.array:
        """
        Creates 2d tensor and returns eigenvalues and eigenvectors.
        """
        tensor = np.array([
            [mx, mxy/2],
            [mxy/2, my]
        ])
        return np.linalg.eig(tensor)

    # -------------------------------------------------------------------------------------------    
    # PLOT METHODS
    # -------------------------------------------------------------------------------------------    

    def plotContour(self, ax: plt.axes, field: str, steps: int or list = 8, **kwargs) -> plt.axes:
        """
        Basic Contour plot.

        args:   - ax = A Matplotlib axes object.
                - field = Field to plot, e.g. 'mx', 'my' or 'mxy'.
                - steps = Nb of contours drawn. (default = 5)
        """
        mvn  = self.data.mvn
        X, Y, Z = self.createGridData(self.xi, self.yi, mvn[field], factor=4)
        unit = self._getUnitfromField(field)

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'${field[0]}_{{{field[1:]}}}$ [{unit}]')
        ax.set_xticks([]); ax.set_yticks([])
        self.plotFrame(ax)

        # Contour plot
        CS = ax.contour(X, Y, Z, steps, colors='grey', **kwargs)
        ax.clabel(CS, inline=1, fontsize=8)

        # Show max/min values
        maxidx = np.unravel_index(Z.argmax(), Z.shape)
        ax.plot(X[maxidx], Y[maxidx], 'k+', markersize=0)
        ax.text(X[maxidx], Y[maxidx], rf'$\leftarrow${Z[maxidx]:.0f}', verticalalignment='center')

        minidx = np.unravel_index(Z.argmin(), Z.shape)
        ax.plot(X[minidx], Y[minidx], 'k+', markersize=0)
        ax.text(X[minidx], Y[minidx], rf'$\leftarrow${Z[minidx]:.0f}', verticalalignment='center')

        return ax


    def plotPrincipal(self, ax: plt.axes, scaling_length: float = 3000, field: str = 'm', **kwargs) -> plt.axes:
        """
        Plot a Vector field of the principal moments.

        args:   - ax = A Matplotlib axes object.
                - data = Dictionary with scattered data in plane.
                - scaling_length = Scaling parameter for velocity plot.
                - field = 'm' or 'n' (Moment or Membrane plot)
        """
        data = self.data.mvn
        # Define keys regarding the desired field
        fx, fy, fxy = f'{field}x', f'{field}y', f'{field}xy'
        # princ, princ_x, princ_y = f'{field}_princ', f'{field}_princ_x', f'{field}_princ_y'
        A = np.vstack((data[fx], data[fy], data[fxy])).T
        x, y = self.xi, self.yi

        wi = np.array([self.getPrincipal(*i)[0] for i in A])
        vi = np.array([self.getPrincipal(*i)[1] for i in A])
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = wi[:, 0].reshape((-1, 1)) * v1, wi[:, 1].reshape((-1, 1)) * v2

        # Grid data for the 2 principal modes
        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        X, Y, Sig = self.createGridData(x, y, wi, interpolation_method='nearest')

        for i, mode in enumerate([Sig1, Sig2]):
            ax.quiver(
                X, Y, mode[:,:,0], mode[:,:,1], 
                color = np.where(Sig[:, :, i]>0, 'red', 'blue').flatten(),
                headwidth=1, headlength=0, headaxislength=0, pivot='mid', scale=scaling_length, width=0.002, minlength=0.1,
                **kwargs
            )

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'${field}_{{princ}}$')
        ax.set_xticks([]); ax.set_yticks([])
        self.plotFrame(ax)
        return ax


    def plotShearflow(self, ax: plt.axes, intensity=5, **kwargs) -> plt.axes:
        """
        Plot the shearflow with streamlines.
        """
        kwargs['color'] = kwargs.get('color', 'grey')

        data = self.data.mvn
        X, Y, VX = self.createGridData(self.xi, self.yi, data['vx'], factor=4, interpolation_method='cubic')
        X, Y, VY = self.createGridData(self.xi, self.yi, data['vy'], factor=4, interpolation_method='cubic')
        VTOT = np.sqrt(VX**2 + VY**2)

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(r'$v_{princ}$')
        ax.set_xticks([]); ax.set_yticks([])
        self.plotFrame(ax)

        # Plot streamline
        intensity = intensity*VTOT/VTOT.max()
        kwargs['density'] = kwargs.get('density', 2)
        ax.streamplot(
            X, Y, -VX, -VY, 
            linewidth=intensity,
            arrowsize=0,
            **kwargs
        )

        return ax


    def plotShearCheck(self, ax: plt.axes, intensity=5, color_critical='red', **kwargs) -> plt.axes:
        """
        Plot shearflow with Ausnutzung
        """
        data = self.data.shear
        y_crit = 379 + 200

        xi, yi = data['x_v'], data['y_v']
        v_aus = np.abs(data['v_0d'] / data['v_rd'])
        v_x, v_y, v_rd = data['v_xd'], data['v_yd'], data['v_rd']

        X, Y, VX = self.createGridData(xi, yi, v_x, factor=4, interpolation_method='linear')
        X, Y, VY = self.createGridData(xi, yi, v_y, factor=4, interpolation_method='linear')
        X, Y, Vaus = self.createGridData(xi, yi, v_aus, factor=4, interpolation_method='linear')
        VTOT = np.sqrt(VX**2 + VY**2)
        Mask = np.where(Vaus>=1, True, False)

        # Manual startpoints to increase density without more lines :(
        ps=np.array([
            np.hstack((X[0], X[-1], X[1:-1,0], X[1:-1, -1])), 
            np.hstack((Y[0], Y[-1], Y[1:-1,0], Y[1:-1, -1]))
            ]).T
        kwargs['density'] = kwargs.get('density', 2)
        kwargs['color'] = kwargs.get('color', 'grey')

        # plot grey streamlines
        self.plotShearflow(ax, intensity=intensity, start_points=ps, **kwargs)

        ax.set_title(r'$v_{princ}$ mit Ausnutzung')

        # Plot streamlines red
        intensity_red = intensity*VTOT/VTOT.max()*Mask
        ax.streamplot(
            X, Y, -VX, -VY, 
            color=color_critical,
            linewidth=intensity_red,
            arrowsize=0,
            start_points=ps,
            **{k: kwargs[k] for k in kwargs.keys() if k!='color'},
        )

        # Plot Nachweisschnitt
        bot_line, top_line = self._ylim[0] + y_crit, self._ylim[1] - y_crit
        ax.hlines(bot_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=1)
        ax.hlines(top_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=1)

        # Show Ausnutzungsziffer
        indices_relevant = np.where((yi<top_line) & (yi>bot_line))[0]
        yy, xx = yi[indices_relevant], xi[indices_relevant]
        ranks = np.argsort(v_aus[indices_relevant])
        
        # top_idx, bot_idx = np.argmin(np.abs(Y[:,0]-top_line)), np.argmin(np.abs(Y[:,0]-bot_line))
        # Xi_top, Xi_bot = X[top_idx, :], X[bot_idx, :]
        # Vaus_top, Vaus_bot = Vaus[top_idx, :], Vaus[bot_idx, :]
        # top_idx_2, bot_idx_2 = Vaus_top.argmax(), Vaus_bot.argmax()

        ax.text(
            xx[ranks[-1]], yy[ranks[-1]], 
            rf'$\leftarrow${v_aus[indices_relevant][ranks[-1]]:.2f}', verticalalignment='center'
        )
        ax.text(
            xx[ranks[-2]], yy[ranks[-2]], 
            rf'$\leftarrow${v_aus[indices_relevant][ranks[-2]]:.2f}', verticalalignment='center'
        )
        

        return ax



    def plotSteel(self, ax: plt.axes, field: str, scaling_length=3000, custom_data=False, color_yield='red', **kwargs):
        """
        Plot the steel stresses at cracks, with info if yielded or not.

        fields: top_x, top_y, bot_x, bot_y
        """
        if custom_data: data = custom_data
        else: data = self.data.steel
        width = kwargs.get('width', 0.005)

        coords, sx, sy, sxy = data[field]
        A = np.vstack((sx, sy, sxy)).T
        x, y = coords[:, 0], coords[:, 1]

        wi = np.array([self.getPrincipal(*i)[0] for i in A])
        vi = np.array([self.getPrincipal(*i)[1] for i in A])
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = wi[:, 0].reshape((-1, 1)) * v1, wi[:, 1].reshape((-1, 1)) * v2

        # Grid data for the 2 principal modes
        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        Yield_1 = np.where(np.abs(Sig1 / self.data.parameters['fsy_P']) >= 1, True, False)
        Yield_2 = np.where(np.abs(Sig2 / self.data.parameters['fsy_P']) >= 1, True, False)

        for (mode, yield_bool) in zip([Sig1, Sig2], [Yield_1, Yield_2]):
            mode_cp = copy(mode)
            mode_cp[yield_bool==False] = 0
            ax.quiver(
                X, Y, mode_cp[:, :, 0], mode_cp[:, :, 1],
                color=color_yield,
                headwidth=1, headlength=0, headaxislength=0, pivot='mid', scale=scaling_length,
                width=2*width,
                minlength=0.1, alpha=.8
            )

        for mode in [Sig1, Sig2]:
            ax.quiver(
                X, Y, mode[:,:, 0], mode[:,:,1], 
                color = kwargs.get('color', 'blue'),
                headwidth=1, headlength=0, headaxislength=0, pivot='mid', scale=scaling_length, width=width, minlength=0.1
            )
        


        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(fr'$\sigma_{{sr,{field}}}$ [N/mm^2]')
        ax.set_xticks([]); ax.set_yticks([])
        self.plotFrame(ax)

        # Show max/min values
        max_principal_stress = np.abs(wi).max(axis=1)
        maxidx = max_principal_stress.argmax()
        ratio = max_principal_stress[maxidx] / self.data.parameters['fsu_P']
        ax.plot(x[maxidx], y[maxidx], 'k+', markersize=0)
        ax.text(x[maxidx], y[maxidx], rf'$\leftarrow${max_principal_stress[maxidx]:.0f} ({ratio:.2f})', verticalalignment='center')

        return ax


    def plotConcrete(self, ax: plt.axes, field: str, scaling_length=3000, custom_data=False, width=0.002, width_scaler=5, **kwargs) -> plt.axes:
        """
        Plot the concrete compression in the plate.
        """
        if custom_data: data = custom_data[field]
        else: data = self.data.conc[field]
        
        x, y = data['coords'].T
        c1, c3, eps = data['sigma_c1'], data['sigma_c3'], data['eps']
        
        wi = np.array([self.getPrincipal(*i)[0] for i in eps])
        vi = np.array([self.getPrincipal(*i)[1] for i in eps])
        
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = c1.reshape((-1, 1)) * v1, c3.reshape((-1, 1)) * v2

        # Ausnutzung
        fcc = self.data.parameters['fcd_P']
        eps_1 = wi[:, 0]
        eps_1[eps_1 <= 0] = 0
        fcc_vec = -(fcc**(2/3) / (0.4 + 30*eps_1) )
        ratio = c3 / fcc_vec

        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        X, Y, C3 = self.createGridData(x, y, c3, interpolation_method='nearest')
        X, Y, C1 = self.createGridData(x, y, c1, interpolation_method='nearest')

        max_width, min_width = width_scaler*width, width

        # This is an extra loop -> Slow, but with thickness intensity
        for mode, value in zip([Sig1, Sig2], [C1, C3]):
            for (xi, yi, u, v, val) in zip(X.flatten(), Y.flatten(), mode[:, :, 0].flatten(), mode[:, :, 1].flatten(), value.flatten()):
                ax.quiver(
                    xi, yi, u, v, 
                    color = 'forestgreen' if val<0 else 'red',
                    headwidth=1, headlength=0, headaxislength=0, pivot='mid', scale=scaling_length, 
                    width= val/c3.min() * (max_width - min_width) + min_width, 
                    minlength=0.1,
                    **kwargs
                )
            # ax.quiver(
            #     X, Y, mode[:,:, 0], mode[:,:,1], 
            #     color = np.where(value < 0, 'blue', 'red').flatten(),
            #     headwidth=1, headlength=0, headaxislength=0, pivot='mid', scale=scaling_length, 
            #     width=0.002, 
            #     minlength=0.1,
            #     **kwargs
            # )
        

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'$\sigma_{{c3,{field}}}$ [N/mm^2]')
        ax.set_xticks([]); ax.set_yticks([])
        self.plotFrame(ax)

        # Show minimum
        minidx = c3.argmin()
        ax.plot(x[minidx], y[minidx], 'k+', markersize=0)
        ax.text(x[minidx], y[minidx], rf'$\leftarrow${c3[minidx]:.0f}', verticalalignment='center')

        # Show max ratio
        ratioidx = ratio.argmax()
        ax.plot(x[ratioidx], y[ratioidx], 'kx', markersize=5)
        ax.text(x[ratioidx], y[ratioidx], rf'{ratio[ratioidx]:.2f}$\rightarrow$', va='center', ha='right')

        return ax


        