import numpy as np

from createSim import createSim

fck = 50

kRiss = np.linspace(0.1, 1.5, 10)

fsy = 550
fsu = 580
fcd = 28
taucd = np.sqrt(fck) * .3 / 1.5

esu = 0.045

ki = .1
# for ki in kRiss:
parameters = {
        'kRiss_P':      ki,
        'fsy_P':        fsy,
        'fsu_P':        fsu,
        'fcd_P':        fcd,
        'fck_P':        fck,
        'tau_cd':       taucd,
        'D_max':        32,
        'ecu_P':        -0.002,
        'eu_act_P':     esu,
        'stepsize_P':   0.10,
        'start_eta':    1.5
    }

createSim(par=parameters)
