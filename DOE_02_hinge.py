import numpy as np

from createSim import createSim

fck = 50

kRiss = 0.1

fsy = [450, 550]
fsu = [470, 580]
fcd = [50, 59]
taucd = np.sqrt(fcd) * .3
esu = [.085, .100]

for i, _ in enumerate(fcd):

    parameters = {
            'kRiss_P':      kRiss,
            'fsy_P':        fsy[i],
            'fsu_P':        fsu[i],
            'fcd_P':        fcd[i],
            'fck_P':        fck,
            'tau_cd':       taucd[i],
            'D_max':        32,
            'ecu_P':        -0.002,
            'eu_act_P':     esu[i],
            'stepsize_P':   0.10,
            'start_eta':    1.5,
            'BC_type':      1 # 1 = hinge, from now on
        }

    createSim(par=parameters)