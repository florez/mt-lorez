import numpy as np

from createSim import createSim

from uqpylab import sessions

mysession = sessions.cloud()
mysession.reset()
uq = mysession.cli
uq.rng(12345)


InpOpts = {
    'Marginals': [
        {
            'Name': 'fcc',
            'Type': 'Uniform',
            'Parameters': [28, 70]
        },
        {
            'Name': 'taucd',
            'Type': 'Uniform',
            'Parameters': [1.0, 3.0]
        },
        {
            'Name': 'kRiss',
            'Type': 'Uniform',
            'Parameters': [0.1, 1.0]
        },
        {
            'Name': 'fsy',
            'Type': 'Uniform',
            'Parameters': [390, 650]
        },
        {
            'Name': 'esu',
            'Type': 'Uniform',
            'Parameters': [.045, .120]
        }
    ]
}

myInput = uq.createInput(InpOpts)

X = uq.getSample(N=100, Method='LHS')

# ------------------------------------------------------------
fck     = 50

fcd     = X[:, 0]
taucd   = X[:, 1]
kRiss   = X[:, 2]
fsy     = X[:, 3]
esu     = X[:, 4]

fsu     = 1.05*fsy
ecu     = -0.002


for i in range(X.shape[0]):


    parameters = {
            'kRiss_P':      kRiss[i],
            'fsy_P':        fsy[i],
            'fsu_P':        fsu[i],
            'fcd_P':        fcd[i],
            'fck_P':        fck,
            'tau_cd':       taucd[i],
            'D_max':        32,
            'ecu_P':        ecu,
            'eu_act_P':     esu[i],
            'stepsize_P':   0.10,
            'start_eta':    1.8
        }

    createSim(par=parameters)