import numpy as np
import os, sys

from PyCMM import ANSYSBatchSimple

from makeFigures import makePlots


def run(path_to):
    Caller = ANSYSBatchSimple(name=os.path.basename(path_to), work_dir=path_to)
    Caller.launchAnsysBatch()

    makePlots(path_to)


if __name__=='__main__':
    
    if len(sys.argv)==2:
        path_to = sys.argv[1]
    else:
        path_to = os.path.abspath('./')

    run(path_to)