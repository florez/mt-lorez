import numpy as np

fck = 50
fcd = 68

parameters = {
        'kRiss_P':      1.0,
        'fsy_P':        600,
        'fsu_P':        650,
        'fcd_P':        fcd,
        'fck_P':        fck,
        'tau_cd':       .3*np.sqrt(fcd),
        'D_max':        32,
        'ecu_P':        -0.002,
        'eu_act_P':     0.125,
        'stepsize_P':   0.10,
        'start_eta':    1.5
    }

