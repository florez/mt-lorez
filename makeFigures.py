from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import sys, os

from PyCMM import ResultReader, MVNPlotter


def makePlots(path_to_folder):

    # ------------------------------------------------------
    # PLOT ALL
    # ------------------------------------------------------


    # Load results
    Reader = ResultReader(path_to_folder)
    Reader.readMVN()
    Reader.readSteel()
    Reader.readConcrete()
    Reader.readShear()

    # Init plotter
    plotter = MVNPlotter(Reader, ndivx = 25, ndivy = 14)

    # Set folder and names for figs
    figure_path = f'{path_to_folder}/figures' # path for figures
    Path(figure_path).mkdir(parents=True, exist_ok=True)
    base_name = os.path.normpath(path_to_folder).split(os.sep)[-1]

    # Concrete plots
    # -------------------------------------------
    fig, axes = plt.subplots(1, 2, figsize=(16, 10))

    plotter.plotConcrete(axes[0], 'bot', scaling_length=100)
    plotter.plotConcrete(axes[1], 'top', scaling_length=100)

    plt.savefig(f'{figure_path}/{base_name}_sig_c3.pdf', facecolor='w')

    # Steel plots
    # -------------------------------------------
    fig, axes = plt.subplots(2, 2, figsize=(16,10))

    plotter.plotSteel(axes[0, 0], field='bot_y', scaling_length=2000)
    plotter.plotSteel(axes[0, 1], field='bot_x', scaling_length=2000)
    plotter.plotSteel(axes[1, 0], field='top_x', scaling_length=2000)
    plotter.plotSteel(axes[1, 1], field='top_y', scaling_length=2000)

    plt.savefig(f'{figure_path}/{base_name}_sig_sr.pdf', facecolor='w')

    # Membrane plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], 'nx')
    plotter.plotContour(axes[0, 1], 'ny')
    plotter.plotContour(axes[1, 0], 'nxy')
    plotter.plotPrincipal(axes[1, 1], scaling_length=3000, field='n')

    plt.savefig(f'{figure_path}/{base_name}_n.pdf', facecolor='w')

    # Shear plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], 'vx')
    plotter.plotContour(axes[0, 1], 'vy')
    plotter.plotContour(axes[1, 0], 'v0')
    plotter.plotShearflow(axes[1, 1], intensity=5)

    plt.savefig(f'{figure_path}/{base_name}_v.pdf', facecolor='w')

    # Shear check plot
    # -------------------------------------------
    fig = plt.figure(figsize=(16, 10))
    ax = fig.add_subplot(111)

    plotter.plotShearCheck(ax, intensity=5, density=7)
    
    plt.savefig(f'{figure_path}/{base_name}_v_check.pdf', facecolor='w')

    # Moment plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], 'mx')
    plotter.plotContour(axes[0, 1], 'my')
    plotter.plotContour(axes[1, 0], 'mxy')
    plotter.plotPrincipal(axes[1,1], scaling_length=3000, field='m')

    plt.savefig(f'{figure_path}/{base_name}_m.pdf', facecolor='w')


    return None


if __name__=='__main__':

    if len(sys.argv)!=2:
        sys.exit('Missing argument! usage: ./makeFigures.py path_to_folder')

    path_to_folder = sys.argv[1]
    
    makePlots(path_to_folder)